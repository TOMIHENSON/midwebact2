//API link
const apiKey = 'http://ergast.com/api/f1/circuits.json?limit=1000';

fetch(apiKey)
.then(response => {
    if (!response.ok) {
        //if something is wrong with API server
        throw new Error(`Network error. ${response.status}`);
    }
    return response.json()
})
.then(data => {
    //assign parsed data into a variable
    const circuits = data.MRData.CircuitTable.Circuits;

    for (const circuit of circuits) {
        const circuitCard = createCircuitCard(circuit);

        //append circuit card to the card container
        const cardContainer = document.getElementById('card-container');
        cardContainer.append(circuitCard);
    }
})
.catch (error => {
    console.error('Error fetching API', error);
});


const searchInput = document.getElementById('search-input');
searchInput.addEventListener('input', function() {
    const keyword = searchInput.value.toLowerCase().trim(); 
    const circuitCards = document.querySelectorAll('.circuit-card');
    circuitCards.forEach(function(circuitCard) {
        const circuitNameElement = circuitCard.querySelector('.circuit-name');
        const circuitName = circuitNameElement.textContent.toLowerCase();
        if (circuitName.includes(keyword)) {
            circuitCard.style.display = 'flex';
        } else {
            circuitCard.style.display = 'none';
        }
    });
});

//array of all countries for dropdown option
var countries = [
    'Australia',
    'Morocco',
    'UK',
    'USA',
    'Sweden',
    'Germany',
    'Bahrain',
    'Azerbaijan',
    'Portugal',
    'Switzerland',
    'India',
    'Spain',
    'France',
    'South Africa',
    'Hungary',
    'Italy',
    'Argentina',
    'Turkey',
    'Saudi Arabia',
    'Singapore',
    'Monaco',
    'Canada',
    'Belgium',
    'Austria',
    'Mexico',
    'Malaysia',
    'China',
    'Russia',
    'UAE',
    'Korea',
    'Netherlands'
];

const countryDropdown = document.getElementById('country-dropdown');

countries.forEach(function(country) {
    const option = document.createElement('option');
    option.value = country;
    option.text = country;
    countryDropdown.appendChild(option);
});

countryDropdown.addEventListener('change', function() {
    const selectedCountry = countryDropdown.value.toLowerCase();

    const circuitCards = document.querySelectorAll('.circuit-card');

    circuitCards.forEach(function(circuitCard) {
        const countryElement = circuitCard.querySelector('.circuit-country');
        const circuitCountry = countryElement.textContent.toLocaleLowerCase();
        if (selectedCountry === 'all' || circuitCountry.includes(selectedCountry)) {
            circuitCard.style.display = 'flex';
        } else {
            circuitCard.style.display = 'none';
        }
    });

    //clear search input if user selected a country
    searchInput.value = '';
});


/**Generates HTML Elements for each attribute of circuit object */
function createCircuitCard(circuit) {
    //get the attributes of a specific circuit object
    const circuitName = circuit.circuitName;
    const locality = circuit.Location.locality;
    const country = circuit.Location.country;
    const latCoordinate = circuit.Location.lat;
    const longCoordinate = circuit.Location.long;
    const coordinate = `Lattitude: ${latCoordinate} Longitude: ${longCoordinate}`;
    const wikiURL = circuit.url;

    //create card for each circuit
    const circuitCard = document.createElement('div');
    circuitCard.setAttribute('class', 'circuit-card');

    //Create element for each attribute
    const nameElement = createElement('p', 'circuit-name', `Circuit Name: ${circuitName}`);
    const localityElement = createElement('p', 'circuit-locality', `Locality: ${locality}`);
    const countryElement = createElement('p', 'circuit-country', `Country: ${country}`);
    const coordinateElement = createElement('p', 'circuit-coordinate', `${coordinate}`);
    const wikiButton = createElement('button', 'circuit-wiki', `View Details`);
    wikiButton.addEventListener('click', function() {
        window.open(wikiURL, '_blank');
    });
    
    //append element to the circuit card
    circuitCard.appendChild(nameElement);
    circuitCard.appendChild(localityElement);
    circuitCard.appendChild(countryElement);
    circuitCard.appendChild(coordinateElement);
    circuitCard.appendChild(wikiButton);

    return circuitCard;
}


/** Creates an HTML Element*/
function createElement(element, attributeValue, textContent) {
    const newElement = document.createElement(element);
    newElement.setAttribute('class', attributeValue);
    newElement.textContent = textContent;
    
    return newElement;
}