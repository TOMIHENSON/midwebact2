

function fetchAndDisplayData(apiUrl) {
    fetch(apiUrl)
        .then(response => response.text())
        .then(xmlData => {
            const xmlDoc = new DOMParser().parseFromString(xmlData, "text/xml");
            const drivers = xmlDoc.querySelectorAll("Driver");

            const f1Drivers = document.getElementById("f1_drivers");

            drivers.forEach(driver => {
                const driverId = driver.getAttribute("driverId");
                const givenName = driver.querySelector("GivenName").textContent;
                const familyName = driver.querySelector("FamilyName").textContent;
                const dateOfBirth = driver.querySelector("DateOfBirth").textContent;
                const nationality = driver.querySelector("Nationality").textContent;
                const profile = driver.getAttribute("url");

                const flipCard = document.createElement('div');
                flipCard.classList.add('flip-card');

                const flipCardInner = document.createElement('div');
                flipCardInner.classList.add('flip-card-inner');

                const flipCardFront = document.createElement('div');
                flipCardFront.classList.add('flip-card-front');

                const query = `${givenName} ${familyName} driver`;
                const serpapiConfig = {
                    q: query,
                    engine: "google_images",
                    ijn: "0",
                    api_key: "e7ece80be0f9ee26b571511f87f2e5b40089b6ac7d523caaf32e8d508ed7411e"
                };

                fetch(`https://serpapi.com/search.json?q=${serpapiConfig.q}&engine=${serpapiConfig.engine}&ijn=${serpapiConfig.ijn}&api_key=${serpapiConfig.api_key}`)
                    .then(response => response.json())
                    .then(json => {
                        const imagesData = json["images_results"];
                        if (imagesData.length > 0) {
                            const driverImageURL = imagesData[0].original;

                            const driverImage = document.createElement('img');
                            driverImage.classList.add('driverImage');
                            driverImage.src = driverImageURL; 
                            driverImage.alt = "Driver Image";
                            flipCardFront.appendChild(driverImage);
                        }
                    })
                    .catch(error => {
                        console.error("Error fetching driver images:", error);
                    });

                const date = new Date(dateOfBirth);
                const options = { year: 'numeric', month: 'long', day: 'numeric' };
                const formattedDateOfBirth = date.toLocaleString('en-US', options);

                const driverIdParagraph = document.createElement('p');
                driverIdParagraph.classList.add('driverId');
                driverIdParagraph.textContent = driverId.toUpperCase();

                flipCardFront.appendChild(driverIdParagraph);

                const flipCardBack = document.createElement('div');
                flipCardBack.classList.add('flip-card-back');

                const fNameParagraph = document.createElement('p');
                fNameParagraph.classList.add('fName');
                fNameParagraph.textContent = "Given Name: " + givenName;

                const lNameParagraph = document.createElement('p');
                lNameParagraph.classList.add('lName');
                lNameParagraph.textContent = "Family Name: " + familyName;

                const bDateParagraph = document.createElement('p');
                bDateParagraph.classList.add('bDate');
                bDateParagraph.textContent = "Date of Birth: " + formattedDateOfBirth; //modified by Vincent Dela Cruz

                const nationalityParagraph = document.createElement('p');
                nationalityParagraph.classList.add('nationality');
                nationalityParagraph.textContent = "Nationality: " + nationality;




                const profileLink = document.createElement('a');
                profileLink.classList.add('profile-link');
                profileLink.href = profile;
                profileLink.target = "_blank";
                profileLink.textContent = "Profile";

                flipCardBack.appendChild(fNameParagraph);
                flipCardBack.appendChild(lNameParagraph);
                flipCardBack.appendChild(bDateParagraph);
                flipCardBack.appendChild(nationalityParagraph);
                flipCardBack.appendChild(profileLink);

                flipCardInner.appendChild(flipCardFront);
                flipCardInner.appendChild(flipCardBack);

                flipCard.appendChild(flipCardInner);

                f1Drivers.appendChild(flipCard);
            });
        })
        .catch(error => console.error('Error:', error));
}

const apiUrl = "http://ergast.com/api/f1/drivers";
fetchAndDisplayData(apiUrl);

function sortDrivers(option) {
    const f1Drivers = document.getElementById("f1_drivers");
    const drivers = Array.from(f1Drivers.children);

    switch (option) {
        case "youngest":
            drivers.sort((a, b) => {
                const dateA = new Date(a.querySelector('.bDate').textContent.replace("Date of Birth: ", ""));
                const dateB = new Date(b.querySelector('.bDate').textContent.replace("Date of Birth: ", ""));
                return dateB - dateA; //modified by Vincent Dela Cruz
            });
            break;

        case "oldest":
            drivers.sort((a, b) => {
                const dateA = new Date(a.querySelector('.bDate').textContent.replace("Date of Birth: ", ""));
                const dateB = new Date(b.querySelector('.bDate').textContent.replace("Date of Birth: ", ""));
                return dateA - dateB; //modified by Vincent Dela Cruz
            });
            break;

        case "alphabeticalA":
            drivers.sort((a, b) => {
                const nameA = a.querySelector('.fName').textContent.replace("Given Name: ", "").toLowerCase();
                const nameB = b.querySelector('.fName').textContent.replace("Given Name: ", "").toLowerCase();
                return nameA.localeCompare(nameB);
            });
            break;

        case "alphabeticalB":
            drivers.sort((a, b) => {
                const nameA = a.querySelector('.fName').textContent.replace("Given Name: ", "").toLowerCase();
                const nameB = b.querySelector('.fName').textContent.replace("Given Name: ", "").toLowerCase();
                return nameB.localeCompare(nameA);
            });
            break;
        
        default:
            break;
    }

    f1Drivers.innerHTML = '';
    drivers.forEach(driver => f1Drivers.appendChild(driver));
}


const sortOptions = document.getElementById("sortOptions");

sortOptions.addEventListener("change", () => {
    const selectedOption = sortOptions.value;
    sortDrivers(selectedOption);
});
