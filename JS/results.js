// // Fetch the XML data
// fetch('http://ergast.com/api/f1/current/last/results')
//   .then((response) => response.text())
//   .then((xmlText) => {
//     // Parse the XML response
//     const parser = new DOMParser();
//     const xmlDoc = parser.parseFromString(xmlText, 'text/xml');

//     // Extract race results information
//     const results = xmlDoc.querySelectorAll('ResultsList Result');

//     // Create a table for displaying the race results
//     const table = document.createElement('table');
//     table.classList.add('race-results');

//     // Create table header
//     const thead = document.createElement('thead');
//     thead.innerHTML = `
//       <tr>
//         <th>Pos</th>
//         <th>No</th>
//         <th>Driver</th>
//         <th>Constructor</th>
//         <th>Laps</th>
//         <th>Grid</th>
//         <th>Time
//         <select class="filter">
//             <option value="asc">Asc</option>
//             <option value="desc">Desc</option>
//         </select>
//         </th>
//         <th>Status
//         <select class="status-filter">
//             <option value="all">All</option>
//             <option value="Finished">Finished</option>
//             <option value="Collision Damage">Collision Damage</option>
//             <option value="Withdrew">Withdrew</option>
//             <option value="Accident">Accident</option>
//           </select>
//         <th>
//         Points
//         <select class="filter">
//             <option value="asc">Asc</option>
//             <option value="desc">Desc</option>
//         </select>
//         </th>
//       </tr>
//     `;
//     table.appendChild(thead);

//     // Create table body
//     const tbody = document.createElement('tbody');

//     // Loop through race results and add rows to the table
//     results.forEach((result) => {
//       // Add conditional checks to handle missing elements
//       const positionElement = result.getAttribute('position');
//       const position = positionElement ? positionElement : 'N/A';
      
//       const numberElement = result.getAttribute('number');
//       const number = numberElement ? numberElement : 'N/A';
      
//       const driverElement = result.querySelector('Driver GivenName');
//       const driverGivenName = driverElement ? driverElement.textContent : 'N/A';
      
//       const familyNameElement = result.querySelector('Driver FamilyName');
//       const driverFamilyName = familyNameElement ? familyNameElement.textContent : 'N/A';
      
//       const driver = driverGivenName + ' ' + driverFamilyName;
      
//       const constructorElement = result.querySelector('Constructor Name');
//       const constructor = constructorElement ? constructorElement.textContent : 'N/A';
      
//       const lapsElement = result.querySelector('Laps');
//       const laps = lapsElement ? lapsElement.textContent : 'N/A';
      
//       const gridElement = result.querySelector('Grid');
//       const grid = gridElement ? gridElement.textContent : 'N/A';
      
//       const timeElement = result.querySelector('Time');
//       const time = timeElement ? timeElement.textContent : 'N/A';
      
//       const statusElement = result.querySelector('Status');
//       const status = statusElement ? statusElement.textContent : 'N/A';
      
//       const pointsElement = result.getAttribute('points');
//       const points = pointsElement ? pointsElement : 'N/A';

//       const row = document.createElement('tr');
//       row.innerHTML = `
//         <td>${position}</td>
//         <td>${number}</td>
//         <td>${driver}</td>
//         <td>${constructor}</td>
//         <td>${laps}</td>
//         <td>${grid}</td>
//         <td>${time}</td>
//         <td>${status}</td>
//         <td>${points}</td>
//       `;

//       tbody.appendChild(row);
//     });

//     table.appendChild(tbody);

//     // Append the table to the "race-results" div
//     document.getElementById('race-results').appendChild(table);
//   })
//   .catch((error) => console.error('Error fetching data:', error));

//    // Function to filter rows based on the selected status
//    function filterByStatus(status) {
//     const rows = tbody.querySelectorAll('tr');
//     rows.forEach((row) => {
//       const rowStatus = row.querySelector('td:nth-child(8)').textContent.trim();
//       if (status === 'all' || rowStatus === status) {
//         row.style.display = 'table-row';
//       } else {
//         row.style.display = 'none';
//       }
//     });
//   }


fetch('http://ergast.com/api/f1/current/last/results')
  .then((response) => response.text())
  .then((xmlText) => {
    // Parse the XML response
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(xmlText, 'text/xml');

    // Extract race results information
    const results = xmlDoc.querySelectorAll('ResultsList Result');

    // Create a table for displaying the race results
    const table = document.createElement('table');
    table.classList.add('race-results');

    // Create table header
    const thead = document.createElement('thead');
    thead.innerHTML = `
        <tr>
        <th>Pos</th>
        <th>No</th>
        <th>Driver</th>
        <th>Constructor</th>
        <th>Laps</th>
        <th>Grid</th>
        <th>Time</th>
        <th>Status
        <select class="status-filter">
            <option value="all">All</option>
            <option value="Finished">Finished</option>
            <option value="Collision damage">Collision Damage</option>
            <option value="Withdrew">Withdrew</option>
            <option value="Accident">Accident</option>
          </select>
        </th>
        <th>Points</th>
      </tr>
    `;
    table.appendChild(thead);

    // Create table body
    const tbody = document.createElement('tbody');


      tbody.innerHTML = ''; // Clear the table body

      results.forEach((result) => {
        // Add conditional checks to handle missing elements
        const positionElement = result.getAttribute('position');
        const position = positionElement ? positionElement : 'N/A';

        const numberElement = result.getAttribute('number');
        const number = numberElement ? numberElement : 'N/A';

        const driverElement = result.querySelector('Driver GivenName');
        const driverGivenName = driverElement ? driverElement.textContent : 'N/A';

        const familyNameElement = result.querySelector('Driver FamilyName');
        const driverFamilyName = familyNameElement ? familyNameElement.textContent : 'N/A';

        const driver = driverGivenName + ' ' + driverFamilyName;

        const constructorElement = result.querySelector('Constructor Name');
        const constructor = constructorElement ? constructorElement.textContent : 'N/A';

        const lapsElement = result.querySelector('Laps');
        const laps = lapsElement ? lapsElement.textContent : 'N/A';

        const gridElement = result.querySelector('Grid');
        const grid = gridElement ? gridElement.textContent : 'N/A';

        const timeElement = result.querySelector('Time');
        const time = timeElement ? timeElement.textContent : 'N/A';

        const statusElement = result.querySelector('Status');
        const status = statusElement ? statusElement.textContent : 'N/A';

        const pointsElement = result.getAttribute('points');
        const points = pointsElement ? pointsElement : 'N/A';

        const row = document.createElement('tr');
        row.innerHTML = `
          <td>${position}</td>
          <td>${number}</td>
          <td>${driver}</td>
          <td>${constructor}</td>
          <td>${laps}</td>
          <td>${grid}</td>
          <td>${time}</td>
          <td>${status}</td>
          <td>${points}</td>
        `;

        tbody.appendChild(row);
      });


    table.appendChild(tbody);

    // Append the table to the "race-results" div
    document.getElementById('race-results').appendChild(table);


  // Add an event listener for the status filter
  document.querySelector('.status-filter').addEventListener('change', (e) => {
    const selectedStatus = e.target.value;
    filterTableByStatus(selectedStatus);
  });

  function filterTableByStatus(status) {
    const rows = Array.from(tbody.querySelectorAll('tr'));

    rows.forEach((row) => {
      const statusCell = row.querySelector('td:nth-child(8)').textContent;

      if (status === 'all' || statusCell.includes(status)) {
        row.style.display = 'table-row'; // Display matching rows
      } else {
        row.style.display = 'none'; // Hide non-matching rows
      }
    });
  }
  })
  .catch((error) => console.error('Error fetching data:', error));