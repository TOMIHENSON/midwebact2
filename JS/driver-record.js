/**
 * API  : Joaquin Raphael Decena
 * Search and Filter : Kate Cadano     
 */
// Fetch the XML data
fetch('https://ergast.com/api/f1/current/driverStandings')
  .then((response) => response.text())
  .then((xmlText) => {
    // Parse the XML response
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(xmlText, 'text/xml');

    // Extract driver standings information
    const driverStandings = xmlDoc.querySelectorAll('DriverStanding');

    // Create a table for displaying the driver standings
    const table = document.createElement('table');
    table.classList.add('driver-standings');

    // Create table header
    const thead = document.createElement('thead');
    thead.innerHTML = `
      <tr>
        <th>
          Pos
          <select class="filter">
            <option value="asc">Asc</option>
            <option value="desc">Desc</option>
          </select>
        </th>
        <th>
          Driver
          <select class="filter">
            <option value="asc">Asc</option>
            <option value="desc">Desc</option>
          </select>
        </th>
        <th>
          Constructor
          <select class="filter">
            <option value="asc">Asc</option>
            <option value="desc">Desc</option>
          </select>
        </th>
        <th>
          Points
          <select class="filter">
            <option value="asc">Asc</option>
            <option value="desc">Desc</option>
          </select>
        </th>
        <th>
          Wins
          <select class="filter">
            <option value="asc">Asc</option>
            <option value="desc">Desc</option>
          </select>
        </th>
      </tr>
    `;
    table.appendChild(thead);

    // Create table body
    const tbody = document.createElement('tbody');

    // Loop through driver standings and add rows to the table
    driverStandings.forEach((standing) => {
      const driver = standing.querySelector('Driver');
      const constructor = standing.querySelector('Constructor');
      const position = standing.getAttribute('position');
      const points = standing.getAttribute('points');
      const wins = standing.getAttribute('wins');
      const driverName = `${driver.querySelector('GivenName').textContent} ${driver.querySelector('FamilyName').textContent}`;
      const constructorName = constructor.querySelector('Name').textContent;

      const row = document.createElement('tr');
      row.innerHTML = `
        <td>${position}</td>
        <td>${driverName}</td>
        <td>${constructorName}</td>
        <td>${points}</td>
        <td>${wins}</td>
      `;

      tbody.appendChild(row);
    });

    table.appendChild(tbody);

    // Append the table to the "driver-standings" div
    document.getElementById('driver-standings').appendChild(table);

    // Add event listeners for sorting
    addEventListenersForSorting(table);


    const searchInput = document.getElementById('search-input');
    
    searchInput.addEventListener('input', function () {
        const searchTerm = searchInput.value.trim().toLowerCase();

        const driverRows = document.querySelectorAll('.driver-standings tbody tr');

        driverRows.forEach((row) => {
            const driverName = row.querySelector('td:nth-child(2)').textContent.toLowerCase();

            if (driverName.includes(searchTerm)) {
                row.style.display = 'table-row'; // Show matching rows
            } else {
                row.style.display = 'none'; // Hide non-matching rows
            }
        });
    });
  })
  .catch((error) => console.error('Error fetching data:', error));

// Function to add event listeners for sorting
function addEventListenersForSorting(table) {
  // Get the table and tbody
  const tbody = table.querySelector('tbody');

  // Get all the filter dropdowns
  const filterDropdowns = table.querySelectorAll('.filter');

  // Function to sort the table
  function sortTable(columnIndex, direction) {
    const rows = Array.from(tbody.querySelectorAll('tr'));
    rows.sort((a, b) => {
      const aValue = a.children[columnIndex].textContent;
      const bValue = b.children[columnIndex].textContent;
      if (isNaN(aValue)) {
        return direction === 'asc' ? aValue.localeCompare(bValue) : bValue.localeCompare(aValue);
      } else {
        return direction === 'asc' ? parseFloat(aValue) - parseFloat(bValue) : parseFloat(bValue) - parseFloat(aValue);
      }
    });
    rows.forEach((row) => tbody.appendChild(row));
  }

  // Add event listeners to filter dropdowns
  filterDropdowns.forEach((filter, index) => {
    filter.addEventListener('change', (event) => {
      const columnIndex = index;
      const direction = event.target.value;
      sortTable(columnIndex, direction);
    });
  });
}
